<?php

/**
 * @author Kanany Hesham <hesham.kanany@gmail.com>
 */
class Dealer
{
    /**
     * Name of the dealer
     * @var string
     */
    private $name;

    /**
     * Phone of the dealer
     * @var string
     */
    private $phone;

    /**
     * Address of the dealer
     * @var string
     */
    private $address;

    /**
     * Url of the dealer
     * 
     * @var string
     */
    private $homepage;

    /**
     *  list of cars 
     * 
     * @var Car[]
     */
    private $cars = array();

    /**
     *
     *set dealer name
     *@param $name
     */
    public function set_name($name){
        $this->name = $name;
    }

    /**
     *
     *get dealer name
     *@return name
     */
    public function get_name(){
        return $this->name;
    }

    /**
     *
     *set dealer phone
     *@param $phone
     */
    public function set_phone($phone){
        $this->phone = $phone;
    }

    /**
     *
     *get dealer phone
     *@return phone
     */
    public function get_phone(){
        return $this->phone;
    }

    /**
     *
     *set dealer address
     *@param $address
     */
    public function set_address($address){
        $this->address = $address;
    }

    /**
     *
     *get dealer address
     *@return address
     */
    public function get_address(){
        return $this->address;
    }

    /**
     *
     *set dealer homepage
     *@param $homepage
     */
    public function set_homepage($homepage){
        $this->homepage = $homepage;
    }

    /**
     *
     *get dealer homepage
     *@return string
     */
    public function get_homepage(){
        return $this->homepage;
    }

    /**
     *
     *set dealer cars
     *@param $cars
     */
    public function set_cars($cars){
        if (!is_array($cars)) {
            $cars = array($cars);
        }
        foreach ($cars as $car) {
            array_push($this->cars, $car);
        }
    }

    /**
     *
     *reset dealer cars
     *@param $cars
     */
    public function reset_cars($cars){
        $this->cars = array();
        if (!is_array($cars)) {
            $cars = array($cars);
        }
        foreach ($cars as $car) {
            array_push($this->cars, $car);
        }
    }

    /**
     *
     *get dealer cars
     *@return array
     */
    public function get_cars(){
        return $this->cars;
    }
}