<?php


/**
 * @author Kanany Hesham <hesham.kanany@gmail.com>
 */
class Car
{
    /**
     * Description text for the car
     * 
     * @var string
     */
    private $description;

    /**
     * Price in euro
     * 
     * @var float
     */
    private $price;

    /**
     *
     *set car description
     *@var string
     */
    public function set_description($description){
        $this->description = $description;
    }

    /**
     *
     *get car description
     *@return description
     */
    public function get_description(){
        return $this->description;
    }

    /**
     *
     *set car price
     *@param $price
     */
    public function set_price($price){
        $this->price = $price;
    }

    /**
     *
     *get car price
     *@return price
     */
    public function get_price(){
        return $this->price;
    }

}
