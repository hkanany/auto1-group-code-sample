<?php

/**
 * @author Kanany Hesham <hesham.kanany@gmail.com>
 */

include "../Service/DealerService.php";


class DealerServiceTest{
    
	/**
	* get all dealers
	* @return array
	*/
	function testGetDealers(){
		$dealerService = new DealerService();
        $dealers = $dealerService->getDealers();
        return $dealers;
	}

	/**
	* get all dealers sorted by cars prices
	* @param $dealers
	* @return array
	*/
	function testGetDealersSortedByCarsPrices($dealers){
		$dealerService = new DealerService();
        $dealers = $dealerService->sortDealersByCarPrices($dealers);
        return $dealers;
	}

}

$testObject = new DealerServiceTest();

$dealers = $testObject->testGetDealers();
echo '//////////// Get all dealers //////////';
echo '<pre>';
print_r($dealers);
echo '</pre>';
echo '<hr>';

$sorted_by_price = $testObject->testGetDealersSortedByCarsPrices($dealers);
echo '//////////// Sort dealers per cars prices //////////';
echo '<pre>';
print_r($sorted_by_price);
echo '</pre>';
