<?php

/**
 * @author Kanany Hesham <hesham.kanany@gmail.com>
 */
interface DealerServiceInterface
{
    /**
     * This method should read dealers data from json 
     *
     * @return \Auto1\Entity\Dealer[]
     */
    public function getDealers();

    /**
     * This method should sort dealers by cars prices
     * @param \Auto1\Entity\Dealer[]
     * @return \Auto1\Entity\Dealer[]
     */
    public function sortDealersByCarPrices($dealers);
}