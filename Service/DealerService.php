<?php

/**
 * @author Kanany Hesham <hesham.kanany@gmail.com>
 */

include("../Service/DealerServiceInterface.php");
include "../Entity/Dealer.php";
include "../Entity/Car.php";

class DealerService implements DealerServiceInterface{

    /**
    * list all dealers 
    * @return array
    */
	public function getDealers() {
		$json_file = __DIR__ . '/../dealers.json';
		 $dealers = array();
        if (is_file($json_file)) {
            $json_data =  json_decode(file_get_contents($json_file), true);
            if (!empty($json_data)) {
                // get dealers data
                foreach ($json_data['dealers'] as $dealer) {
                    $dealer_object = new Dealer();
                    $dealer_object->set_name($dealer['name']);
                    $dealer_object->set_homepage($dealer['homepage']);
                    $dealer_object->set_phone($dealer['phone']);
                    $dealer_object->set_address($dealer['address']);
                    // Cars
                    if (!empty($dealer['cars'])) {
                        foreach ($dealer['cars'] as $car) {
                            $car_object = new Car();
                            $car_object->set_description($car['description']);
                            $car_object->set_price($car['price']);
                            $dealer_object->set_cars($car_object);
                        }
                    }
                    $dealers[] = $dealer_object;
                }
                
            }
            return $dealers;
        }
	}

    /**
    * order dealers by car price
    * @param $dealers
    * @return array
    */
    public function sortDealersByCarPrices($dealers){
        usort($dealers, function ($a, $b) {
            return $this->getLowestPricePerDealer($a) > $this->getLowestPricePerDealer($b);
        });

        return $dealers;
    }

    /**
     * Get lowest price per dealers
     * @param $dealers
     * @return array
     */
    private function getLowestPricePerDealer($dealer) {
        $cars = $dealer->get_cars();
        if (count($cars) == 0) return null;

        usort($cars, function ($a, $b) {
            return $a->get_price() > $b->get_price();
        });

        $dealer->reset_cars($cars);        

        $lowestPrice = $cars[0];
        return $lowestPrice;
    }

}